# README #

This is the repository for the c# program I wrote for the Youi team as a coding exercise.



### Setup ###

Just clone this repository on a Widows computer and you are good to go.

### File Format ###

I've assumed the file received by e-mail as the correct format. The file contained a first line with the column titles, and 4 columns in this order: FirstName, LastName, Address, PhoneNumber. 

### Running the code ###

This is a **console application**, so after you build the .exe, you need to run it via command line, passing the path to the CSV file as a parameter. You can pass the delimiter as a second parameter, but I've assumed the comma as a default.

### Test Project ###

I've used **Nunit** for unit testing. You can find the project and the test cases in the solution.


If you have any questions you can e-mail me on joaortk@gmail.com


Thank you!