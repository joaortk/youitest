﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestProgram
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                Console.WriteLine("File must be informed!");
            }
            else
            {
                var file = args[0];
                var delimiter = args.Length > 1 ? args[1] : ","; // comma is default
                FileHelper fh = new FileHelper();
                var data = fh.ParseCSVFile(file, delimiter);

                DataAnalysisHelper analyzer = new DataAnalysisHelper();
                fh.OutputFile(analyzer.GetFrequenciesString(data), AppDomain.CurrentDomain.BaseDirectory + "Frequencies.txt");
                fh.OutputFile(analyzer.GetAddressesString(data), AppDomain.CurrentDomain.BaseDirectory + "Addresses.txt");

            }
        }
    }
}
