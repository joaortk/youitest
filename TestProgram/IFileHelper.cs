﻿using System;
using System.Collections.Generic;
using TestProgram.Model;

namespace TestProgram
{
	public interface IFileHelper
	{
		IList<Person> ParseCSVFile(string path, string delimiter);
		bool OutputFile(string content, string fileName);
	}
}
