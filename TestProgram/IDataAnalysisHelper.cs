﻿using System;
using System.Collections.Generic;
using TestProgram.Model;
using static TestProgram.DataAnalysisHelper;

namespace TestProgram
{
    public interface IDataAnalysisHelper
    {
        IEnumerable<FrequencyName> GetFirstNameFrequency(IList<Person> data);
        IEnumerable<FrequencyName> GetLastNameFrequency(IList<Person> data);
        IEnumerable<string> GetAddressesOrdered(IList<Person> data);
    }
}
