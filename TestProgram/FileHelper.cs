﻿using System;
using System.Collections.Generic;
using System.IO;
using TestProgram.Model;
using Microsoft.VisualBasic.FileIO;

namespace TestProgram
{
	public class FileHelper : IFileHelper
	{
		/// <summary>
		/// Parses the file with the given delimiter
		/// </summary>
		/// <returns>The list of people in the file</returns>
		/// <param name="path">File Path.</param>
		/// <param name="delimiter">Delimiter.</param>
		public IList<Person> ParseCSVFile(string path, string delimiter)
		{
			var data = new List<Person>();
			using (TextFieldParser parser = new TextFieldParser(path))
			{
				parser.TextFieldType = FieldType.Delimited;
				parser.SetDelimiters(delimiter);
				//ignore first line with titles
				parser.ReadFields();
				while (!parser.EndOfData)
				{
					var fields = parser.ReadFields();
					if (fields.Length != 4)
					{
						throw new FormatException("File is not in the right format!");
					}
					else {
						data.Add(new Person
						{
							FirstName = fields[0],
							LastName = fields[1],
							Address = fields[2],
							PhoneNumber = fields[3]
						});
					}
				}
			}

			return data;
		}

		/// <summary>
		/// Prints to file.
		/// </summary>
		/// <returns><c>true</c>, if to file was printed, <c>false</c> otherwise.</returns>
		/// <param name="content">The Content to be written on the file.</param>
		/// <param name="fileName">The File Name.</param>
		public bool OutputFile(string content, string fileName)
		{
			try
			{
				using (StreamWriter writer = new StreamWriter(fileName))
				{
					writer.Write(content);
				}
                Console.WriteLine("File saved -> " + fileName);
			}
			catch (Exception e)
			{
				return false;
			}
			return true;
		}
	}
}
