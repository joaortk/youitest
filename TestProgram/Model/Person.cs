﻿using System;
namespace TestProgram.Model
{
	public class Person
	{
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string Address { get; set; }
		public string PhoneNumber { get; set; }

		public override bool Equals(object obj)
		{
			if (obj is Person)
			{
				return FirstName.Equals(((Person)obj).FirstName) &&
								LastName.Equals(((Person)obj).LastName) &&
								Address.Equals(((Person)obj).Address) &&
								PhoneNumber.Equals(((Person)obj).PhoneNumber);
			}
			else {
				return false;
			}
		}
		public override int GetHashCode()
		{
			return base.GetHashCode();
		}
	}
}
