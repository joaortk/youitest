﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestProgram.Model
{
    public class FrequencyName
    {
        public double Frequency { get; set; }
        public string Name { get; set; }

        public override bool Equals(object obj)
        {
            return obj is FrequencyName &&
                Frequency.Equals(((FrequencyName)obj).Frequency) &&
                Name.Equals(((FrequencyName)obj).Name);
        }
    }
}
