﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TestProgram.Model;
using System.Text;

namespace TestProgram
{
    public class DataAnalysisHelper : IDataAnalysisHelper
    {

        public string GetFrequenciesString(IList<Person> data)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("First Names ordered by Frequency");
            sb.AppendLine();
            var queryfirstName = GetFirstNameFrequency(data);

            foreach (var item in queryfirstName)
            {
                sb.AppendLine(string.Format("First name = {0}, Frequency = {1}", item.Name, item.Frequency));
                Console.WriteLine("First name = {0}, Frequency = {1}", item.Name, item.Frequency);
            }


            sb.AppendLine();
            sb.AppendLine("Last Names ordered by Frequency");
            sb.AppendLine();
            var queryLastName = GetLastNameFrequency(data);

            foreach (var item in queryLastName)
            {
                sb.AppendLine(string.Format("Last name = {0}, Frequency = {1}", item.Name, item.Frequency));
                Console.WriteLine("Last name = {0}, Frequency = {1}", item.Name, item.Frequency);
            }

            return sb.ToString();
        }

        public string GetAddressesString(IList<Person> data)
        {

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Addresses ordered by Street Name");
            sb.AppendLine();
            var queryAddresses = GetAddressesOrdered(data);

            foreach (var item in queryAddresses)
            {
                sb.AppendLine(item);
                Console.WriteLine("Address = {0}", item);
            }
            return sb.ToString();

        }

        public IEnumerable<string> GetAddressesOrdered(IList<Person> data)
        {
            return from d in data
                   orderby d.Address.Split(' ')[1]
                   select d.Address;
        }

        public IEnumerable<FrequencyName> GetFirstNameFrequency(IList<Person> data)
        {
            return from d in data
                   group d by d.FirstName into f
                   orderby (double)f.Count() / data.Count, f.Key
                   select new FrequencyName { Frequency = (double)f.Count() / data.Count, Name = f.Key };
        }

        public IEnumerable<FrequencyName> GetLastNameFrequency(IList<Person> data)
        {
            return from d in data
                   group d by d.LastName into f
                   orderby (double)f.Count() / data.Count, f.Key
                   select new FrequencyName { Frequency = (double)f.Count() / data.Count, Name = f.Key };
        }



    }
    
}
