﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using TestProgram.Model;

namespace TestProgram.Tests
{
    [TestFixture]
    public class FileHelperTest
    {
        IFileHelper fileHelper;

        [OneTimeSetUp]
        public void TestSetup()
        {
            fileHelper = new FileHelper();

        }

        [Test]
        public void ShouldParseCSVFile()
        {
            //var path = "ms-appx:///assets/data.csv";
            var path = AppDomain.CurrentDomain.BaseDirectory + "assets/data.csv";
            var delimiter = ",";
            var expected = new List<Person> {
                 new Person{FirstName = "Jimmy" , LastName = "Smith", Address = "102 Long Lane"   , PhoneNumber = "29384857" }
                ,new Person{FirstName = "Clive" , LastName = "Owen" , Address = "65 Ambling Way"  , PhoneNumber = "31214788" }
                ,new Person{FirstName = "James" , LastName = "Brown", Address = "82 Stewart St"   , PhoneNumber = "32114566" }
                ,new Person{FirstName = "Graham", LastName = "Howe" , Address = "12 Howard St"    , PhoneNumber = "8766556"  }
                ,new Person{FirstName = "John"  , LastName = "Howe" , Address = "78 Short Lane"   , PhoneNumber = "29384857" }
                ,new Person{FirstName = "Clive" , LastName = "Smith", Address = "49 Sutherland St", PhoneNumber = "31214788" }
                ,new Person{FirstName = "James" , LastName = "Owen" , Address = "8 Crimson Rd"    , PhoneNumber = "32114566" }
                ,new Person{FirstName = "Graham", LastName = "Brown", Address = "94 Roland St"    , PhoneNumber = "8766556"  }
            };
            var data = fileHelper.ParseCSVFile(path, delimiter);
            CollectionAssert.AreEqual(expected, data);
        }
        [Test]
        public void ShouldNotFindFile()
        {
            var path = AppDomain.CurrentDomain.BaseDirectory + "assets/notfound.csv";
            var delimiter = ",";
            Assert.Throws<FileNotFoundException>(() => fileHelper.ParseCSVFile(path, delimiter));

        }


        [OneTimeTearDown]
        public void TestTearDown()
        {
            fileHelper = null;
        }
    }
}
