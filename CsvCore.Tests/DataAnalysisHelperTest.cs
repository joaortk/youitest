﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestProgram;
using TestProgram.Model;

namespace CsvCore.Tests
{
    [TestFixture]
    class DataAnalysisHelperTest
    {
        IDataAnalysisHelper dataHelper;

        [OneTimeSetUp]
        public void TestSetup()
        {
            dataHelper = new DataAnalysisHelper();
        }

        [Test]
        public void ShouldGetFrequencies()
        {
            var data = new List<Person> {
                new Person{FirstName = "John" , LastName = "Doe", Address = "52 Long Lane"   , PhoneNumber = "29384857" },
                new Person{FirstName = "Jane" , LastName = "Doe", Address = "52 Long Lane"   , PhoneNumber = "29384857" },
                new Person{FirstName = "Jane" , LastName = "Doe", Address = "52 Street"      , PhoneNumber = "29384857" },
                new Person{FirstName = "Jane" , LastName = "Doe", Address = "52 Long Lane"   , PhoneNumber = "29384857" },
                new Person{FirstName = "Jack" , LastName = "Doe", Address = "52 Long Lane"   , PhoneNumber = "29384857" },
                new Person{FirstName = "Jack" , LastName = "Doe", Address = "52 Street"      , PhoneNumber = "29384857" },
                new Person{FirstName = "Jane" , LastName = "Doe", Address = "52 Street"      , PhoneNumber = "29384857" },
                new Person{FirstName = "John" , LastName = "Doe", Address = "52 Street"      , PhoneNumber = "29384857" },
                new Person{FirstName = "Jane" , LastName = "Doe", Address = "52 Long Lane"   , PhoneNumber = "29384857" },
                new Person{FirstName = "Jane" , LastName = "Doe", Address = "52 Street"      , PhoneNumber = "29384857" }
            };


            var expected = new List<FrequencyName> {
                new FrequencyName { Name = "Jack", Frequency = .2},
                new FrequencyName { Name = "John", Frequency = .2},
                new FrequencyName { Name = "Jane", Frequency = .6}

            };

            CollectionAssert.AreEqual(expected, dataHelper.GetFirstNameFrequency(data).ToList());
        }

        [Test]
        public void ShouldGetOrderedAddresses()
        {
            var data = new List<Person> {
                new Person{FirstName = "John" , LastName = "Doe", Address = "34 Sugar"   , PhoneNumber = "29384857" },
                new Person{FirstName = "John" , LastName = "Doe", Address = "75 Alice"      , PhoneNumber = "29384857" },
                new Person{FirstName = "John" , LastName = "Doe", Address = "43 Marjorie"   , PhoneNumber = "29384857" },
                new Person{FirstName = "John" , LastName = "Doe", Address = "32 Ocean View"      , PhoneNumber = "29384857" },
                new Person{FirstName = "John" , LastName = "Doe", Address = "23 Goonawarra"   , PhoneNumber = "29384857" },
                new Person{FirstName = "Jane" , LastName = "Doe", Address = "324 Brentwood"      , PhoneNumber = "29384857" },
                new Person{FirstName = "Jane" , LastName = "Doe", Address = "56 Dalby"   , PhoneNumber = "29384857" },
                new Person{FirstName = "Jane" , LastName = "Doe", Address = "798 Crystal Waves"      , PhoneNumber = "29384857" },
                new Person{FirstName = "Jane" , LastName = "Doe", Address = "23 Bungama"   , PhoneNumber = "29384857" },
                new Person{FirstName = "Jane" , LastName = "Doe", Address = "321 Allambie"      , PhoneNumber = "29384857" }
            };

            var expected = new List<string> {
                 "75 Alice"      ,
                 "321 Allambie",
                 "324 Brentwood",
                 "23 Bungama"   ,
                 "798 Crystal Waves",
                 "56 Dalby"   ,
                 "23 Goonawarra"   ,
                 "43 Marjorie"   ,
                 "32 Ocean View",
                 "34 Sugar"   
            };

            CollectionAssert.AreEqual(expected, dataHelper.GetAddressesOrdered(data));
        }

        [OneTimeTearDown]
        public void TestTearDown()
        {
            dataHelper = null;
        }
    }
}
